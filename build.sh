#!/bin/sh

FILES=$CI_PROJECT_DIR/scripts/*.user.js
CATENATED=''
for f in $FILES
do
  echo "Processing $f file..."
  filename=$(basename -- "$(echo "$f" | sed -e 's^ ^%20^g')")
  # sed -i -e '#^// @supportURL.*#a // @downloadURL https://nagyv-gitlab.gitlab.io/tampermonkey-scripts/$filename' "$f"
  CATENATED="$filename;$CATENATED"
done
echo $CATENATED | jq --slurp --raw-input 'split(";")[:-1]' > scripts/files.json