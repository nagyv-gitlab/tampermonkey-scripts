// ==UserScript==
// @name         Use last author name when replying
// @namespace    https://gitlab.com/
// @version      0.3
// @description  Use the last uathor name when replying to issue comments
// @author       Viktor Nagy
// @homepage     https://nagyv-gitlab.gitlab.io/tampermonkey-scripts/
// @supportURL   https://gitlab.com/nagyv-gitlab/tampermonkey-scripts
// @match        https://gitlab.com/*/issues/*
// @match        https://gitlab.com/*/-/merge_requests/*
// @grant        none
// @require      https://code.jquery.com/jquery-3.4.1.slim.min.js
// ==/UserScript==

(function() {
    'use strict';

    const handler = function(ev) {
        console.log('Running last author name handler')
        const textarea = $('#note_note');
        // find the last reply
        const replies = $('.discussion-reply-holder').closest('ul').find('li.note');
        const lastReply = replies[replies.length-1];
        const username = $(lastReply).find('.js-user-link').attr('data-username');
        console.log('Found username', username);
        textarea.val(`@${username} `)
    }

    const findReplyButtons = function() {
        if( $('li.note-discussion').length > 0) {
            window.setTimeout(() => $('button.js-vue-discussion-reply').click(handler), 1000);
        }
    }
    window.setTimeout(()=> $('button[data-track-label="reply_comment_button"]').click(handler), 2000);
    window.setTimeout(findReplyButtons, 2000);

})();
