// ==UserScript==
// @name         Handy GitLab actions
// @namespace    https://gitlab.com/
// @version      0.3
// @description  Add quick actions under the main comment and new issue textblock.
// @author       Viktor Nagy
// @homepage     https://nagyv-gitlab.gitlab.io/tampermonkey-scripts/
// @supportURL   https://gitlab.com/nagyv-gitlab/tampermonkey-scripts
// @match        https://gitlab.com/*/issues/new*
// @match        https://gitlab.com/*/issues/*
// @grant        none
// @require      https://code.jquery.com/jquery-3.4.1.slim.min.js
// ==/UserScript==

(function() {
    'use strict';
    const actions = [
        ['/assign me', 'Mine'],
        ['/label ~"group::system" ~"workflow::problem validation"', 'Default labels'],
        ['/label ~"workflow::solution validation"', 'Solution label'],
        ['/label ~"workflow::validation backlog"\n/milestone %Backlog', 'Backlog'],
        ['/label ~documentations', 'Needs docs'],
        ['/assign @nicholasklick', 'Nicholas'],
    ]

    const lastEpic = window.localStorage.getItem('last-epic');
    if(lastEpic) {
      const [epicId, epicTitle] = JSON.parse(lastEpic);
      actions.push([`/epic &${epicId}`, `Epic: ${epicTitle}`]);
    }

    function handleAction (ev) {
        ev.preventDefault()
        if(ev.target.dataset.action) {
            addAction(ev.target.dataset.action)
        }
    }

    function addAction(actionText) {
        descriptionField.val(`${descriptionField.val()}\n${actionText}`)
        descriptionField.scrollTop(descriptionField[0].scrollHeight);
    }

    function getDescriptionField() {
        const parts = window.location.href.split('?')[0].split('/')
        if(parts[parts.length -1] === 'new') {
            return $('textarea#issue_description')
        } else {
            return $('textarea#note-body')
        }
    }

    const descriptionField = getDescriptionField()
    descriptionField.closest('.md-write-holder').append('Quick actions: ' + actions.map(([action, text]) => `<button type="button" class="vnagy-handyactions btn btn-link" data-action='${action}'>${text}</button>`).join(', '))

    $('.vnagy-handyactions').click(handleAction)
})();
